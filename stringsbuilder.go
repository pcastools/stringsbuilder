// Stringsbuilder provides a pool of strings.Builders.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package stringsbuilder

import (
	"strings"
	"sync"
)

// pool is a pool of strings.Builders ready for use.
var pool = &sync.Pool{
	New: func() interface{} {
		return &strings.Builder{}
	},
}

/////////////////////////////////////////////////////////////////////////
// Public functions
/////////////////////////////////////////////////////////////////////////

// New returns a new strings.Builder.
func New() *strings.Builder {
	return pool.Get().(*strings.Builder)
}

// NewBytes returns a new strings.Builder using buf as its initial contents.
func NewBytes(buf []byte) *strings.Builder {
	b := New()
	b.Write(buf)
	return b
}

// NewString returns a new strings.Builder using s as its initial contents.
func NewString(s string) *strings.Builder {
	b := New()
	b.WriteString(s)
	return b
}

// Reuse returns the given strings.Builder to a pool ready for reuse.
func Reuse(b *strings.Builder) {
	b.Reset()
	pool.Put(b)
}
